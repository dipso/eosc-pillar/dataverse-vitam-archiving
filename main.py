import requests
import script
import scrap
import concurrent.futures
import sys
import os
from dotenv import load_dotenv

load_dotenv()

# dataverseDownloadFileUrl = "https://data-test.jouy.inra.fr/api/access/datafile/:persistentId/?persistentId=doi:"
# dataverseBaseUrl = "https://data-test.jouy.inra.fr/api"
dataverseDownloadFileUrl = script.dataverseDownloadFileUrl
dataverseBaseUrl = script.dataverseBaseUrl
slash_url_escape_value = "-"
slash = "/"
two_point = ":"
ftpUrl = os.environ.get('ftpUrl')
logs = open('logs.txt', 'w')


def getDatasetMetadata(datasetId):
   # Extract PId
    if two_point in datasetId:
        datasetId_tab = datasetId.split(two_point)
        if len(datasetId_tab) >= 2:
            datasetId = datasetId_tab[1]

    completeResourceUrl = dataverseBaseUrl + \
        "/datasets/:persistentId/?persistentId=doi:"+datasetId
    print(completeResourceUrl)
    response = requests.get(completeResourceUrl, verify=False)
    print(completeResourceUrl)
    data = response.json()
    return data['data']


def import_data_from_dataverse(datasetPid):

    if two_point in datasetPid:
        datasetPid_tab = datasetPid.split(two_point)
        if len(datasetPid_tab) >= 2:
            datasetPid = datasetPid_tab[1]

    # download_dataverse_file(filePid, fileMetadata.fileName)
    script.generate_xml(getDatasetMetadata(datasetPid))


class DataverseFileMetadata:
    fileName = ""
    description = ""

    def __init(self, fileLabel, fileDescription):
        self.fileName = fileLabel
        self.description = fileDescription


# scrapin for IDs :
# def listScrap():
    # get root directory url first page

    # iterate on url page number

    # from beautiful soup append ids to the list


doi = "doi:"


def manual_mode():
    okmode = False
    while okmode == False:

        mode = input(
            "Mode selection : \n1. Single DOI\n2. DOI list file\nEnter 1 or 2 : ")

        if (mode == "1"):

            doiNext = input("\nEnter doi ( format xx.xxxxx/xxxxxx) : ")
            # 10.70112/WMM0QD
            import_data_from_dataverse(doi+doiNext)
            okmode = True
        elif (mode == "2"):
            filename = input("\nEnter filename: ")
            list = scrap.getList(filename)
            print("Archiving " + str(len(list)) + " datasets...")
            logs.write("Archiving " + str(len(list)) + " datasets :\n\n")
            # for x in list:
            #     import_data_from_dataverse(x)
            #     print(x)

            def try_my_operation(item):

                try:
                    import_data_from_dataverse(item)
                    return {"pid": item, "status": "archived"}
                except Exception as e:
                    print('error with ' + item + e)
                    return {"pid": item, "status": e}

            executor = concurrent.futures.ProcessPoolExecutor(8)
            # futures = [executor.submit(try_my_operation, item)
            #            for item in list]
            # concurrent.futures.wait(futures)
            pids = []
            for r in executor.map(try_my_operation, list):
                pids.append(r)
            print(pids)
            for i in pids:
                logs.write(str(i["pid"]) + ' : ' + i["status"] + "\n")
            okmode = True
        else:
            print("\nWrong mode")


def continuous_mode():
    list = scrap.getList("list.txt")
    print("Archiving " + str(len(list)) + " datasets to "+ftpUrl + "...")
    logs.write("Archiving " + str(len(list)) + " datasets :\n\n")

    pids = []
    for x in list:
        import_data_from_dataverse(x)
        print(x)
        pids.append(x)
    print(pids)

    # def try_my_operation(item):

    #     try:
    #         import_data_from_dataverse(item)
    #         return {"pid": item, "status": "archived"}
    #     except Exception as e:
    #         print('error with ' + item + e)
    #         return {"pid": item, "status": e}

    # executor = concurrent.futures.ProcessPoolExecutor(1)
    # futures = [executor.submit(try_my_operation, item)
    #            for item in list]
    # concurrent.futures.wait(futures)

    # for r in executor.map(try_my_operation, list):
    #     pids.append(r)
    print(pids)
    for i in pids:
        logs.write(str(i["pid"]) + ' : ' + i["status"] + "\n")


def test_mode():
    doiNext = os.environ.get('testDOI')
    print('archiving test DOI', doiNext)
    import_data_from_dataverse(doi+doiNext)


if __name__ == "__main__":
    if sys.argv[1] == "c":
        print("continuous mode")
        continuous_mode()
    elif sys.argv[1] == "m":
        print('manual mode')
        manual_mode()
    elif sys.argv[1] == "t":
        print("test mode :")
        test_mode()
