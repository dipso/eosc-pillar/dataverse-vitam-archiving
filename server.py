from flask import Flask, make_response, render_template, Response

app = Flask(__name__)
app.debug = True


def make_logs():
    logs = open('logs.txt', 'r')
    data = logs.read().replace('\n', '')
    return data


@app.route("/logstxt")
def getLogsTxt():
    # with open("outputs/Adjacency.csv") as fp:
    #     csv = fp.read()
    logs = open('logs.txt', 'r')
    return Response(
        logs,
        mimetype="text/txt",
        headers={"Content-disposition":
                 "attachment; filename=logs.txt"})


@app.route("/logs", methods=['GET'])
def display_logs():
    # response = make_response(make_logs(), 200)
    # response.mimetype = "text/plain"
    logs = open('logs.txt', 'r')
    return render_template('index.html', your_list=logs)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
