# syntax=docker/dockerfile:1

FROM python:3.7
RUN mkdir /app
WORKDIR /app

COPY requirements.txt /app
RUN pip install --upgrade pip
RUN pip3 install -r requirements.txt

COPY . /app

EXPOSE 80
COPY .env .env
CMD ["python", "server.py"]
