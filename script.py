from lxml import etree
import requests
import os
import tarfile
import shutil
import csv
import ftplib
import os
from dotenv import load_dotenv
import pysftp
import re
import urllib3

requests.packages.urllib3.disable_warnings()
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += ':HIGH:!DH:!aNULL'
try:
    requests.packages.urllib3.contrib.pyopenssl.util.ssl_.DEFAULT_CIPHERS += ':HIGH:!DH:!aNULL'
except AttributeError:
    # no pyopenssl support used / needed / available
    pass

load_dotenv()

dataverseDownloadFileUrl = "https://entrepot.recherche.data.gouv.fr/api/access/datafile/:persistentId/?persistentId=doi:"
dataverseBaseUrl = "https://entrepot.recherche.data.gouv.fr/api"
ftpUrl = os.environ.get('ftpUrl')
ftpLogin = os.environ.get('ftpLogin')
ftppwd = os.environ.get('ftppwd')

fornats = ['.aif', '.aiff', '.ogg', '.flac', '.gif', '.tiff', '.jp2', '.jpeg',
           '.matroska', '.mp4', '.txt', '.text', '.pdf', '.png', '.svg', '.tei', '.wav', '.xml']

# session = ftplib.FTP(ftpUrl, ftpLogin, ftppwd)
cnopts = pysftp.CnOpts()
cnopts.hostkeys = None
session = pysftp.Connection(host=ftpUrl, username=ftpLogin,
                            password=ftppwd, cnopts=cnopts)

two_point = ":"


def download_dataverse_file(filePid):
    finalDataverseUrl = dataverseDownloadFileUrl+filePid
    response = requests.get(
        finalDataverseUrl, allow_redirects=True, verify=False)

    return response.content


def generate_xml(data):

    newDataset = False

    # if not os.path.exists("output/"):
    #     os.mkdir("output/")

    if (os.path.isfile(str(data['id'])+'.csv')):

        read = open(str(data['id'])+'.csv', 'r', newline='')
        reader = csv.reader(read)
        lines = list(reader)
    else:
        newDataset = True

    if (not newDataset):
        if (int(lines[0][1]) != data['latestVersion']['id']):
            newDataset = True

    if (newDataset):

        print('updating SIP...')

        file = open(str(data['id'])+'.csv', 'w', newline='')

        writer = csv.writer(file)
        writer.writerow(['dataset', data['latestVersion']['id']])

        attr_qname = etree.QName(
            "http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
        nsmap = {None: "http://www.cines.fr/pac/sip",
                 "stm": "http://www.cines.fr/pac/sip.xsd",
                 "xsi": "http://www.w3.org/2001/XMLSchema-instance"}

        pac = etree.Element('pac', {
            attr_qname: "http://www.cines.fr/pac/sip http://www.cines.fr/pac/sip.xsd"},
            nsmap=nsmap
        )

        def findField(field):
            return next(item for item in data['latestVersion']['metadataBlocks']['citation']['fields'] if item["typeName"] == field)

        def getAuthors():
            authors = []
            for author in findField("author")["value"]:
                if ("authorAffiliation" in author):
                    authors.append(author["authorName"]["value"] +
                                   " (" + author["authorAffiliation"]["value"]+")")
                else:
                    authors.append(author["authorName"]["value"])
            return authors

        DocDC = etree.SubElement(pac, "DocDC")

        title = etree.SubElement(DocDC, 'title')
        title.set("language", "eng")
        title.text = findField("title")["value"]

        authors = getAuthors()
        for idx, val in enumerate(authors):
            idx = etree.SubElement(DocDC, 'creator')
            idx.text = val

        subject = etree.SubElement(DocDC, 'subject')
        subject.set("language", "eng")
        subject.text = findField("subject")["value"][0]

        description = etree.SubElement(DocDC, 'description')
        description.set("language", "eng")
        description.text = findField("dsDescription")[
            "value"][0]["dsDescriptionValue"]["value"]

        publisher = etree.SubElement(DocDC, 'publisher')
        publisher.text = data["publisher"]

        date = etree.SubElement(DocDC, 'date')
        date.text = data["publicationDate"]

        dataType = etree.SubElement(DocDC, 'type')
        dataType.set("language", "eng")
        try:
            dataType.text = findField("kindOfData")["value"][0]
        except StopIteration:
            dataType.text = "multiple"

        dataFormat = etree.SubElement(DocDC, 'format')
        dataFormat.set("language", "eng")
        dataFormat.text = "text/plain"

        language = etree.SubElement(DocDC, 'language')
        language.text = "eng"

        rights = etree.SubElement(DocDC, 'rights')
        rights.set("language", "eng")
        rights.text = '"https://www.etalab.gouv.fr/licence-ouverte-open-licence"Licence Ouverte / Open Licence Version 2.0 compatible CC BY '
        DocMeta = etree.SubElement(pac, "DocMeta")

        identifiantDocProducteur = etree.SubElement(
            DocMeta, 'identifiantDocProducteur')
        identifiantDocProducteur.text = data["persistentUrl"]

        serviceVersant = etree.SubElement(DocMeta, "serviceVersant")
        # if findField("datasetContact")["value"][0]["datasetContactAffiliation"]["value"] == "www.inra.fr":
        #     service = "Institut national de recherche pour l'agriculture, l'alimentation et l'environnement (INRAE)"
        # else:
        #     service = findField("datasetContact")["value"][0][
        #         "datasetContactAffiliation"]["value"]

        service = "Institut national de recherche pour l'agriculture, l'alimentation et l'environnement (INRAE)"

        serviceVersant.text = service
        planClassement = etree.SubElement(DocMeta, 'planClassement')
        planClassement.set("language", "eng")
        planClassement.text = "Data4Pillar"

        version = etree.SubElement(DocMeta, 'version')
        version.text = str(data["latestVersion"]["versionNumber"])

        if os.path.exists(str(data['id'])):
            shutil.rmtree(str(data['id']))
        os.mkdir(str(data['id']))
        os.mkdir(str(data['id'])+"/DEPOT")

        for f in data["latestVersion"]["files"]:
            fichMeta = etree.SubElement(pac, "FichMeta")
            encodage = etree.SubElement(fichMeta, "encodage")
            encodage.text = "UTF-8"
            formatFichier = etree.SubElement(fichMeta, "formatFichier")

            if "." in f["label"]:
                label = f["label"].split(".")
                if len(label) >= 2:
                    format = label[1].upper()
                    if format == "TAB" or format == "tab":
                        format = "TXT"
            else:
                format = "PDF"

            # writer.writerow([f['label'], f['datasetVersionId']])

            formatFichier.text = format
            nomFichier = etree.SubElement(fichMeta, "nomFichier")
            nomFichier.text = f["label"]
            empreinteOri = etree.SubElement(fichMeta, "empreinteOri")
            empreinteOri.set("type", f["dataFile"]["checksum"]["type"])
            empreinteOri.text = f["dataFile"]["checksum"]["value"]
            filePid = f["dataFile"]["persistentId"]
            if two_point in filePid:
                filePid_tab = filePid.split(two_point)
                if len(filePid_tab) >= 2:
                    filePid = filePid_tab[1]

            print(re.split("^[^.]*", f["label"])[1])

            if (fornats.count(re.split("^[^.]*", f["label"])[1]) > 0):
                print("format ok")
                open(str(data['id'])+"/DEPOT/" + f["label"],
                     "wb").write(download_dataverse_file(filePid))
            else:
                print("format not ok")
                if (re.split("^[^.]*", f["label"])[1] == ".tab" or re.split("^[^.]*", f["label"])[1] == ".csv"):
                    print("to text")
                open(str(data['id'])+"/DEPOT/" + re.split("^[^.]*", f["label"])[0] + '.txt',
                     "wb").write(download_dataverse_file(filePid))

        tree = etree.ElementTree(pac)

        tree.write(str(data['id'])+"/sip.xml", xml_declaration=True,
                   encoding='utf-8', pretty_print=True)

        if not os.path.isfile(str(data['id'])+".tar"):
            with tarfile.open(str(data['id'])+".tar", "x") as tar:
                tar.add(str(data['id'])+"/sip.xml", recursive=True)
                tar.add(str(data['id'])+"/DEPOT", recursive=True)

        # file = open("output/"str(data['id'])+".tar", 'rb')
        # send the file

        session.put(str(data['id'])+".tar", "/" +
                    ftpLogin+"/VERS/"+str(data['id'])+".tar")

        # session.close()
        # file.close()
        os.remove(str(data['id'])+".tar")
        shutil.rmtree(str(data['id']))
        logs = open('logs.txt', 'a')
        logs.write(str(data['id']) + " ok\n")
    else:
        print('nothing to update')
