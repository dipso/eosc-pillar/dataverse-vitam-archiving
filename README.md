# Dataverse Vitam Archiving script

This python script prepare a dataverse dataset to the Vitam archiving process.

![](schemaScript.png)

# Installing

This script utilizes python3 with requests and lxml

```
pip install requests
pip install lxml
```

# Configuration

Modify script.py line 7 and 8 to indicate your dataverse server URL

```
dataverseDownloadFileUrl = "https://{{dataverse URL}}/api/access/datafile/:persistentId/?persistentId=doi:"
dataverseBaseUrl = "{{dataverse URL}}"
```

# Running

```
python3 main.py
```

Then specify the dataset persistant id following the xx.xxxxx/xxxxxx format

# future version

Datasets id auto scrapping
